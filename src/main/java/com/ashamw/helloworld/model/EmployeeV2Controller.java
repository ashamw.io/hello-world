package com.ashamw.helloworld.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ashamw.helloworld.service.EmployeeService;
import com.ashamw.helloworld.service.EmployeeV2Service;

@RestController
@RequestMapping("/v2/employees")
public class EmployeeV2Controller {

	// @Qualifier("EmployeeV2ServiceImpl")
	@Autowired
	private EmployeeV2Service service;

	@PostMapping
	public Employee save(@RequestBody Employee employee) {
		return service.save(employee);
	}

	@GetMapping
	public List<Employee> getEmployees() {
		return service.getAllEmployees();
	}

	@GetMapping("/{id}")
	public Employee getEmployeeId(@PathVariable String id) {
		return service.getEmployeeId(id);
	}

	@DeleteMapping("/{id}")
	public String deleteEmployeeById(@PathVariable String id) {
		return service.deleteEmployeeById(id);
	}

}
