package com.ashamw.helloworld.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.ashamw.helloworld.entity.EmployeEntity;
import com.ashamw.helloworld.model.Employee;
import com.ashamw.helloworld.repository.EmployeeRepository;

@Service
@Primary
public class EmployeeV2Service implements EmployeeService {

	@Autowired
	private EmployeeRepository repository;

	@Override
	public Employee save(Employee employee) {
		if (employee.getId() == null || employee.getEmailId().isEmpty()) {
			employee.setId(UUID.randomUUID().toString());
		}
		EmployeEntity entity = new EmployeEntity();
		BeanUtils.copyProperties(employee, entity);
		repository.save(entity);
		return employee;
	}

	@Override
	public List<Employee> getAllEmployees() {
		List<EmployeEntity> employeeEntityList = repository.findAll();

		List<Employee> employees = employeeEntityList.stream().map(employeeEntity -> {
			Employee employee = new Employee();
			BeanUtils.copyProperties(employeeEntity, employee);
			return employee;
		}).collect(Collectors.toList());

		return employees;
	}

	@Override
	public Employee getEmployeeId(String id) {
		EmployeEntity employeeEntity = repository.findById(id).get();
		Employee employee = new Employee();
		BeanUtils.copyProperties(employeeEntity, employee);
		return employee;
	}

	@Override
	public String deleteEmployeeById(String id) {
		repository.deleteById(id);
		return "Employee deleted with the id: " + id;
	}

}
