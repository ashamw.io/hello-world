package com.ashamw.helloworld.service;

import java.util.List;

import com.ashamw.helloworld.model.Employee;

public interface EmployeeService {
	Employee save(Employee employee);

	List<Employee> getAllEmployees();

	Employee getEmployeeId(String id);

	String deleteEmployeeById(String id);
}
