package com.ashamw.helloworld.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.ashamw.helloworld.error.EmployeeNotFoundException;
import com.ashamw.helloworld.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	List<Employee> employees = new ArrayList();

	@Override
	public Employee save(Employee employee) {
		if (employee.getId() == null || employee.getEmailId().isEmpty()) {
			employee.setId(UUID.randomUUID().toString());

		}
		employees.add(employee);
		return employee;
	}

	public List<Employee> getAllEmployees() {
		return employees;
	}

	public Employee getEmployeeId(String id) {
		return employees.stream().filter(employee -> employee.getId().equals(id)).findFirst()
				.orElseThrow(() -> new RuntimeException("" + "Employee not found with id: " + id));
	}

	public String deleteEmployeeById(String id) {
		Employee employee = employees.stream().filter(e -> e.getId().equalsIgnoreCase(id)).findFirst().get();
		employees.remove(employee);
		return "Employee deleted with the id " + id;
	}

}
