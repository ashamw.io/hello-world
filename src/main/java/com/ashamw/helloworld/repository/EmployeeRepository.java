package com.ashamw.helloworld.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ashamw.helloworld.entity.EmployeEntity;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeEntity, String>{

}
