package com.ashamw.helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ashamw.helloworld.model.User;

//
//@Controller
//@ResponseBody
@RestController
public class HomeController {

	@RequestMapping("/")
	public String home() {
		return "Hello World!";
	}

	@GetMapping("/user")
	public User user() {
		User user = new User();
		user.setId("1");
		user.setName("Asha");
		user.setEmailId("asha@gmail.com");
		return user;
	}

	@GetMapping("/{id}/{id1}")
	public String pathVariable(@PathVariable String id,@PathVariable String id1) {
		return ("The path variable is: " + id+" ..."+id1);
	}
	
	@GetMapping("/requestParam") 
	public String requestParams(@RequestParam String name,@RequestParam(name="email",required = false,defaultValue = "") String emailId) {
		return "Your name is: "+name+" and emailid "+ emailId;
	}

}
